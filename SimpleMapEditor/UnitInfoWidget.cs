﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NevendaarTools;
using System.IO;
using System.Drawing.Imaging;
using NevendaarTools.Transform;
using System.Runtime.InteropServices;

namespace SimpleMapEditor
{
    public partial class UnitInfoWidget : UserControl
    {
        MapModel mapModel;
        GameDataModel gameModel;

        public class SearchResult
        {
            public string context;
            public GameUnit unit;
        }
        List<SearchResult> searchResults = new List<SearchResult>();

        public void ReloadTranslation()
        {

        }

        public UnitInfoWidget()
        {
            InitializeComponent();
            TranslationHelper.Instance().TranslationChanged += ReloadTranslation;
        }

        public void Init(MapModel mapModel_,
                 GameDataModel gameModel_)
        {
            mapModel = mapModel_;
            gameModel = gameModel_;
            searchResults.Clear();
            resultListBox.Items.Clear();
            foreach(GameUnit unit in gameModel._Units.Values)
            {
                //if (!unit.objId.EndsWith("8232"))
                //    continue;
                searchResults.Add(new SearchResult()
                {
                    context = unit.name + " - " + unit.objId,
                    unit = unit
                });
                
            }
            searchResults.Sort((x, y) => String.Compare(x.context, y.context));
            foreach (SearchResult res in searchResults)
            {
                resultListBox.Items.Add(res.context);
            }
        }

        Font nameFont = new Font("comic sans", 9, FontStyle.Bold);
        Font unitNameFont = new Font("comic sans", 11, FontStyle.Bold);
        Font baseFont = new Font("comic sans", 9);
        Font extraFont = new Font("comic sans", 9);
        Font immunFont = new Font("comic sans", 8);
        int nameW = 120;
        int offsetX = 140;
        int rowH = 18;

        void DrawTableCell(Graphics g, int x, int y, string name, string value, int rowCount = 1)
        {
            int w = nameW;
            if (name == "")
                w = 0;
            g.DrawString(TranslationHelper.Instance().Tr(name), nameFont, new SolidBrush(Color.Black),
                    new RectangleF(x, y, nameW, rowH * rowCount));
            g.DrawString(value, baseFont, new SolidBrush(Color.Black),
                new RectangleF(x + w, y, 170, rowH * rowCount));
        }
        void DrawTableCell(Graphics g, int x, int y, string name, string value1, string value2, string value3, int rowCount = 1)
        {
            int w = nameW;
            if (name == "")
                w = 0;
            g.DrawString(TranslationHelper.Instance().Tr(name), nameFont, new SolidBrush(Color.Black),
                    new RectangleF(x, y, 170, rowH * rowCount));
            string extra = "";
            if (value2 == "0" && value3 == "0")
                extra = "";
            else
            {
                if (value2 == value3)
                    extra = "(" + value2 + ")";
                else
                {
                    extra = "(" + value2 + " / " + value3 + ")";
                }
            }
            g.DrawString(value1, baseFont, new SolidBrush(Color.Black),
                new RectangleF(x + w, y, 170, rowH * rowCount));
            g.DrawString(extra, extraFont, new SolidBrush(Color.Black),
                new RectangleF(x + w + value1.Length * 8, y, 170, rowH * rowCount));
        }
        void DrawTableCell(Graphics g, int x, int y, string name, string value1, string value2)
        {
            int w = nameW;
            if (name == "")
                w = 0;
            g.DrawString(TranslationHelper.Instance().Tr(name), nameFont, new SolidBrush(Color.Black),
                    new RectangleF(x, y, 140, 30));
            string extra = "";
            if (value2 != "")
                extra = "(" + value2 + ")";
            g.DrawString(value1, baseFont, new SolidBrush(Color.Black),
                new RectangleF(x + w, y, 170, rowH));
            g.DrawString(extra, extraFont, new SolidBrush(Color.Black),
                new RectangleF(x + w + value1.Length * 8, y, 170, rowH));
        }

        public Bitmap CropImage(Bitmap source, Rectangle section)
        {
            var bitmap = new Bitmap(section.Width, section.Height);
            using (var g = Graphics.FromImage(bitmap))
            {
                g.DrawImage(source, 0, 0, section, GraphicsUnit.Pixel);
                return bitmap;
            }
        }

        int DrawCostValues(Graphics g, int x, int y, string name, Image resource, int val1, int val2, int val3)
        {
            if (val1 > 0 || val2 > 0 || val3 > 0)
            {
                g.DrawImage(resource, new Rectangle(x, y, rowH, rowH));
                DrawTableCell(g, x + rowH, y, "", val1.ToString(), UpgrdToString(val2), UpgrdToString(val3));
                return rowH;
            }
            return 0;
        }
        int DrawCostCells(Graphics g, int x, int y, string name, Image resource, string cost1, string cost2 , string cost3)
        {
            int w = nameW;
            if (name == "")
                w = 0;
            else
                g.DrawString(TranslationHelper.Instance().Tr(name), nameFont, new SolidBrush(Color.Black),
                    new RectangleF(x, y, nameW, rowH));
            int G1 = CostHelper.G(cost1);
            int R1 = CostHelper.R(cost1);
            int Y1 = CostHelper.Y(cost1);
            int E1 = CostHelper.E(cost1);
            int W1 = CostHelper.W(cost1);
            int B1 = CostHelper.B(cost1);

            int G2 = CostHelper.G(cost2);
            int R2 = CostHelper.R(cost2);
            int Y2 = CostHelper.Y(cost2);
            int E2 = CostHelper.E(cost2);
            int W2 = CostHelper.W(cost2);
            int B2 = CostHelper.B(cost2);

            int G3 = CostHelper.G(cost3);
            int R3 = CostHelper.R(cost3);
            int Y3 = CostHelper.Y(cost3);
            int E3 = CostHelper.E(cost3);
            int W3 = CostHelper.W(cost3);
            int B3 = CostHelper.B(cost3);

            int newX = x + w;
            int newY = y;
            Bitmap res = new Bitmap(resource);
            newY += DrawCostValues(g, newX, newY, "", CropImage(res, new Rectangle(35 * 2 + 1, 30 * 2 + 2, 36, 30 - 2)), G1, G2, G3);
            newY += DrawCostValues(g, newX, newY, "", CropImage(res, new Rectangle(35 * 1, 31 * 2, 36, 30)), R1, R2, R3);
            newY += DrawCostValues(g, newX, newY, "", CropImage(res, new Rectangle(35 * 1, 30 * 0, 36, 30)), Y1, Y2, Y3);
            newY += DrawCostValues(g, newX, newY, "", CropImage(res, new Rectangle(35 * 0, 30 * 0, 36, 30)), E1, E2, E3);
            newY += DrawCostValues(g, newX, newY, "", CropImage(res, new Rectangle(35 * 2, 30 * 0, 36, 30)), W1, W2, W3);
            newY += DrawCostValues(g, newX, newY, "", CropImage(res, new Rectangle(35 * 2, 30 * 3, 36, 30)), B1, B2, B3);

            return newY;
        }

        Image MakeTransparent(ref Image image)
        {
            if (image.Palette.Entries.Count() > 0)
            {
                ColorPalette palette = image.Palette;
                Color[] entries = palette.Entries;
                entries[0] = Color.Transparent;
                image.Palette = palette;
                return image;
            }
            else
            {
                Bitmap processedBitmap = new Bitmap(image);
                BitmapData bitmapData = processedBitmap.LockBits(new Rectangle(0, 0, processedBitmap.Width, processedBitmap.Height), ImageLockMode.ReadWrite, processedBitmap.PixelFormat);

                int bytesPerPixel = Bitmap.GetPixelFormatSize(processedBitmap.PixelFormat) / 8;
                int byteCount = bitmapData.Stride * processedBitmap.Height;
                byte[] pixels = new byte[byteCount];
                IntPtr ptrFirstPixel = bitmapData.Scan0;
                Marshal.Copy(ptrFirstPixel, pixels, 0, pixels.Length);
                int heightInPixels = bitmapData.Height;
                int widthInBytes = bitmapData.Width * bytesPerPixel;

                for (int y = 0; y < heightInPixels; y++)
                {
                    int currentLine = y * bitmapData.Stride;
                    for (int x = 0; x < widthInBytes; x = x + bytesPerPixel)
                    {
                        byte oldBlue = pixels[currentLine + x];
                        byte oldGreen = pixels[currentLine + x + 1];
                        byte oldRed = pixels[currentLine + x + 2];

                        pixels[currentLine + x + 0] = oldBlue;
                        pixels[currentLine + x + 1] = oldGreen;
                        pixels[currentLine + x + 2] = oldRed;

                        if (oldRed == 255 && oldBlue == 255)
                            pixels[currentLine + x + 3] = 0;
                    }
                }

                // copy modified bytes back
                Marshal.Copy(pixels, 0, ptrFirstPixel, pixels.Length);
                processedBitmap.UnlockBits(bitmapData);
                return processedBitmap;
            }
        }

        string UpgrdToString(int val)
        {
            if (val < 0)
                return val.ToString();
            if (val == 0)
                return "0";
            return "+" + val.ToString();
        }

        string AttackTarget(GameSkill skill)
        {
            if (skill.reachNum == 1)
                return TranslationHelper.Instance().Tr("All targets");
            if (skill.reachNum == 2)
                return TranslationHelper.Instance().Tr("Any target");
            if (skill.reachNum == 3)
                return TranslationHelper.Instance().Tr("Melee target");

            return skill.reach.ToString() + " " + skill.target; 
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = resultListBox.SelectedIndex;
            if (index < 0 || index > searchResults.Count)
                return;
            //Image image = MapTileHelper.ResizeImage(
            //    Image.FromStream(gameModel._ResourceModel.GetImageRawById("_PG0500IXMOD")), new Size(600, 600));
            Image image = Image.FromStream(gameModel._ResourceModel.GetImageRawById("_PG0500IXMOD"));
            image = MakeTransparent(ref image);
            StringFormat sf = new StringFormat();
            sf.LineAlignment = StringAlignment.Center;
            sf.Alignment = StringAlignment.Center;

            GameUnit unit = searchResults[index].unit;
            GameUnitUpgr upgr1 = gameModel._UnitUpgrs[unit.dyn1];
            GameUnitUpgr upgr2 = gameModel._UnitUpgrs[unit.dyn2];
            string unitIconId = (unit.baseUnit == "G000000000") ? unit.objId : unit.baseUnit;
            MemoryStream stream = gameModel._ResourceModel.GetImageRawById(unitIconId.ToUpper() + "FACE");
            Image unitIm = Image.FromStream(stream);
            Image resource = Image.FromStream(gameModel._ResourceModel.GetImageRawById("_RESOURCES"));
            resource = MakeTransparent(ref resource);
            using (Graphics g = Graphics.FromImage(image))
            {
                int posY = 70;
                int firstPartW = image.Width / 2 - offsetX - 40;
                int secondPartW = image.Width - firstPartW - offsetX * 2;
                int posX = offsetX + (firstPartW - unitIm.Width)/2 - 10;
                //g.DrawLine(new Pen(Color.Red), offsetX + firstPartW, 0, offsetX + firstPartW, image.Height);
                g.DrawString(unit.name, unitNameFont, new SolidBrush(Color.Black), 
                    new RectangleF(offsetX - 10, posY + unitIm.Height, firstPartW, 30), sf);
                g.DrawString(unit.desc, baseFont, new SolidBrush(Color.Black),
                    new RectangleF(offsetX, posY + unitIm.Height + 34, firstPartW, 100));
                //g.DrawRectangle(new Pen(Color.Red), new Rectangle(offsetX, posY + unitIm.Height + 34, firstPartW, 86));
                g.DrawImage(unitIm, new Rectangle(posX, posY, unitIm.Width, unitIm.Height));
                posY = posY + unitIm.Height + 34 + 100;
                if (unit.upgradeB != "" && unit.upgradeB.ToUpper() != "G000000000")
                {
                    MemoryStream buildingStream = gameModel._ResourceModel.GetImageRawById("bilding" + unit.upgradeB.ToUpper());
                    Image buildIm = Image.FromStream(buildingStream);
                    int buildX = offsetX;
                    g.DrawString(TranslationHelper.Instance().Tr("Living in:"), baseFont, new SolidBrush(Color.Black),
                        new RectangleF(buildX, posY, 100, rowH));
                    g.DrawImage(buildIm, new Rectangle(buildX, posY + rowH, buildIm.Width, buildIm.Height));
                    GameBuilding building = gameModel._Buildings[unit.upgradeB.ToLower()];
                    
                    g.DrawString(building.name, nameFont, new SolidBrush(Color.Black),
                        new RectangleF(buildX - 10, posY + buildIm.Height + rowH, buildIm.Width + 20, rowH * 2), sf);
                    DrawCostCells(g, buildX + 10, posY + buildIm.Height + rowH * 3, "", resource, building.cost, "", "");
                    
                    if (unit.prev != "" && unit.prev.ToUpper() != "G000000000")
                    {
                        GameUnit prev = gameModel._Units[unit.prev.ToLower()];
                        string prevIconId = (prev.baseUnit == "G000000000") ? prev.objId : prev.baseUnit;
                        MemoryStream prevStream = gameModel._ResourceModel.GetImageRawById(prevIconId.ToUpper() + "FACEB");
                        if (prevStream != null)
                        {
                            Image prevIm = Image.FromStream(prevStream);
                            prevIm = MakeTransparent(ref prevIm);
                            int prevX = buildX + buildIm.Width + 25;
                            g.DrawString(TranslationHelper.Instance().Tr("Trained from:"), baseFont, new SolidBrush(Color.Black),
                                new RectangleF(prevX, posY, 100, rowH));
                            g.DrawImage(prevIm, new Rectangle(prevX + 10, posY + rowH, buildIm.Width, buildIm.Height));
                            

                            g.DrawString(prev.name, nameFont, new SolidBrush(Color.Black),
                                new RectangleF(prevX - 10, posY + buildIm.Height + rowH, buildIm.Width + 20, rowH * 2), sf);
                        }
                    }
                    posY = posY + buildIm.Height + rowH * 4 + rowH / 2;
                }

                int oldNameW = nameW;
                nameW -= 24;
                posY = DrawCostCells(g, offsetX - 10, posY, "Cost:", resource, unit.cost, upgr1.cost, upgr2.cost);
                posY = DrawCostCells(g, offsetX - 10, posY, "ReviveCost:", resource, unit.costRevive, upgr1.costRevive, upgr2.costRevive);
                posY = DrawCostCells(g, offsetX - 10, posY, "HealCost:", resource, unit.costHeal, upgr1.costHeal, upgr2.costHeal);
                nameW = oldNameW;
                posX = offsetX + firstPartW;
                posY = 70;

                DrawTableCell(g, posX, posY += rowH, "Level / threshold:", unit.level.ToString(), unit.dynLvl.ToString());
                DrawTableCell(g, posX, posY += rowH, "Exp:", unit.expNext.ToString(), UpgrdToString(upgr1.expNext), UpgrdToString(upgr2.expNext));
                DrawTableCell(g, posX, posY += rowH, "ExpKill:", unit.expKill.ToString(), UpgrdToString(upgr1.expKill), UpgrdToString(upgr2.expKill));
                posY += rowH / 2;
                DrawTableCell(g, posX, posY += rowH, "HP:", unit.hp.ToString(), UpgrdToString(upgr1.hp), UpgrdToString(upgr2.hp));
                DrawTableCell(g, posX, posY += rowH, "Regen, %:", unit.regen.ToString(), UpgrdToString(upgr1.regen), UpgrdToString(upgr2.regen));
                DrawTableCell(g, posX, posY += rowH, "Armor:", unit.armor.ToString(), UpgrdToString(upgr1.armor), UpgrdToString(upgr2.armor));

                int immunW = secondPartW - nameW;
                string immunities = "";
                foreach (string immun in unit.immunities)
                    immunities += ((immunities == "") ? "" : ", ") + TranslationHelper.Instance().Tr(immun);
                if (immunities == "")
                    immunities = TranslationHelper.Instance().Tr("None");
                int rowCount = (int)Math.Ceiling((double)immunities.Length * 8.5 / (double)(immunW));
                g.DrawString(TranslationHelper.Instance().Tr("Immunity:"), nameFont, new SolidBrush(Color.Black),
                    new RectangleF(posX, posY += 20, nameW, rowH));
                g.DrawString(immunities, immunFont, new SolidBrush(Color.Black),
                    new RectangleF(posX + nameW, posY, immunW, rowH * rowCount));
                posY += rowH * rowCount * 7 / 10 + 10;
                immunities = "";
                foreach (string immun in unit.resistance)
                    immunities += ((immunities == "") ? "" : ", ") + TranslationHelper.Instance().Tr(immun);
                if (immunities == "")
                    immunities = TranslationHelper.Instance().Tr("None");
                rowCount = (int)Math.Ceiling((double)immunities.Length * 8.5 / (double)(immunW));
                g.DrawString(TranslationHelper.Instance().Tr("Resistance:"), nameFont, new SolidBrush(Color.Black),
                    new RectangleF(posX, posY, nameW, rowH));
                g.DrawString(immunities, immunFont, new SolidBrush(Color.Black),
                    new RectangleF(posX + nameW, posY, immunW, rowH * rowCount));
                posY += rowH * rowCount * 7 / 10;

                GameSkill skill = gameModel._Skills[unit.attackId];
                string powerName1 = "";
                string powerName2 = "";
                string attackText = skill.name;
                string attackPower = skill.power.ToString();
                

                GetAttackDamageString(skill, ref powerName1, ref attackPower);
                if (unit.attackTwice)
                    attackText = "(2x) " + attackText;
                
                string attackSource = skill.source;
                string attackAcc = skill.acc.ToString();
                if (unit.attack2Id != "" && gameModel._Skills.ContainsKey(unit.attack2Id))
                {
                    GameSkill skill2 = gameModel._Skills[unit.attack2Id];
                    string attackPower2 = skill2.power.ToString();
                    GetAttackDamageString(skill2, ref powerName2, ref attackPower2);
                    if (attackPower2 != "")
                        attackPower += " / " + attackPower2;

                    attackText += " / " + skill2.name;

                    attackSource += " / " + skill2.source;
                }
                rowCount = (int)Math.Ceiling((double)attackText.Length * 9 / (double)(immunW));
                DrawTableCell(g, posX, posY += rowH, "Attack:", attackText, rowCount);
                DrawTableCell(g, posX, posY += rowH * rowCount, "Source:", attackSource);
                DrawTableCell(g, posX, posY += rowH, "Accuracy:", attackAcc, UpgrdToString(upgr1.acc), UpgrdToString(upgr2.acc));
                DrawTableCell(g, posX, posY += rowH, powerName1, attackPower, UpgrdToString(upgr1.damage), UpgrdToString(upgr2.damage));
                DrawTableCell(g, posX, posY += rowH, "Ini:", skill.ini.ToString(), UpgrdToString(upgr1.ini), UpgrdToString(upgr2.ini));
                DrawTableCell(g, posX, posY += rowH, "Target:", AttackTarget(skill));
                posY += rowH;
            }
            pictureBox1.Image = image;
            image.Save("C://test.png", ImageFormat.Png);
            //Rectangle bounds = this.Bounds;
            //using (Bitmap bitmap = new Bitmap(bounds.Width, bounds.Height))
            //{
            //    using (Graphics g = Graphics.FromImage(bitmap))
            //    {
            //        g.CopyFromScreen(new Point(bounds.Left, bounds.Top), Point.Empty, bounds.Size);
            //    }
            //    //pictureBox1.Image = bitmap;
            //    bitmap.Save("C://test.jpg", ImageFormat.Jpeg);
            //}
        }
        public void GetAttackDamageString(GameSkill skill, ref string name, ref string value)
        {
            if (skill.className == "L_BOOST_DAMAGE")
            {
                name = TranslationHelper.Instance().Tr("Boost:");
                value = (skill.level * 25).ToString() + "%";
                return;
            }
            if (skill.className == "L_LOWER_DAMAGE")
            {
                //name = TranslationHelper.Instance().Tr("Boost:");
                //value = (skill.level * 33).ToString() + "%";
                name = "";
                value = "";
                return;
            }
            if (skill.className == "L_HEAL")
            {
                name = TranslationHelper.Instance().Tr("Heal:");
                value = skill.heal.ToString();
                return;
            }
            if (skill.className == "L_DAMAGE" || skill.className == "L_DRAIN" || skill.className == "L_DRAIN_OVERFLOW" ||
                skill.className == "L_POISON" || skill.className == "L_FROSTBITE" || skill.className == "L_BLISTER" || skill.className == "L_SHATTER")
            {
                name = TranslationHelper.Instance().Tr("Attack:");
                value = skill.power.ToString();
                return;
            }

            name = "";
            value = "";
        }
    }
}
