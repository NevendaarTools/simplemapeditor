﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NevendaarTools;

namespace SimpleMapEditor
{
    public partial class VillageWidget : UserControl
    {
        MapModel mapModel;
        D2Village village;
        GameDataModel gameModel;
        public VillageWidget()
        {
            InitializeComponent();
            TranslationHelper.Instance().TranslationChanged += ReloadTranslation;
        }

        public void ReloadTranslation()
        {

        }

        public void SetObject(ref MapModel map, D2Village village_, ref GameDataModel dataModel)
        {
            mapModel = map;
            village = village_;
            gameModel = dataModel;

            idLabel.Text = village._ObjId;
            nameTextBox.Text = village._Name;
            levelUpDown.Value = village._Size;
            int mapSize = mapModel.Header._mapSize;
            Bitmap buf = MapTileHelper.FillImage(ref mapModel, mapSize);

            for (int i = 0; i < 4; ++i)
            {
                for (int k = 0; k < 4; ++k)
                {
                    buf.SetPixel(village._PosX + i, village._PosY + k, Color.Silver);
                }
            }

            pictureBox1.Image = buf;
        }

        public void Apply()
        {
            village._Name = nameTextBox.Text;
            village._Size = (int)levelUpDown.Value;
            mapModel.Replace(village);
            SetObject(ref mapModel, village, ref gameModel);
        }

        private void applyButton_Click(object sender, EventArgs e)
        {
            Apply();
        }
    }
}
