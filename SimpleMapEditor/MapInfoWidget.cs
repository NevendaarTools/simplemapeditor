﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NevendaarTools.DataTypes;
using NevendaarTools;
using NevendaarTools.Creation;

namespace NevedaarWidgets
{
    public partial class MapInfoWidget: UserControl
    {
        MapModel mapModel = null;
        Bitmap racesImage;
        public MapInfoWidget()
        {
            InitializeComponent();
            TranslationHelper.Instance().TranslationChanged += ReloadTranslation;
        }

        public void ReloadTranslation()
        {
            labelAuthor.Text = TranslationHelper.Instance().Tr("Author:");
            labelName.Text = TranslationHelper.Instance().Tr("Name:");
            labelSizeTitle.Text = TranslationHelper.Instance().Tr("Size:");
            labelFormatTitle.Text = TranslationHelper.Instance().Tr("Format:");
            racesLabel.Text = TranslationHelper.Instance().Tr("Races:");
        }
        
        public void SetMap(ref MapModel map)
        {
            textBoxName.Text = map.Header._Name;
            textBoxAuthor.Text = map.Header._Author;
            labelFormat.Text = map.Header._MapType + " " + map.Header._Version;
            D2Plan plan = map.GetById<D2Plan>("PN0000");
            int mapSize = plan._magicNumber;

            labelSize.Text = mapSize.ToString() + " x " + mapSize.ToString();
            pictureBox1.Image = MapTileHelper.FillImage(ref map, mapSize);
            mapModel = map;
            List<D2Player> players = map.GetListByType<D2Player>();
            int offset = 0;
            racesImage = new Bitmap((players.Count() - 1) * 14, 12);
            foreach(var player in players)
            {
                if (player._raceId == GameRace.Neutral)
                    continue;
                Color color = MapTileHelper.ColorByRace(player._raceId);
                for (int i = 0; i < 12; ++i)
                {
                    for(int k = 0; k < 12; ++k)
                    {
                        if (i==0 || i == 11 || k == 0 || k == 11)
                            racesImage.SetPixel(offset + i, k, Color.Black);
                        else
                            racesImage.SetPixel(offset + i, k, color);
                    }
                }
                offset += 14;
            }
            racesPictureBox.Image = racesImage;
        }

        public string MapName()
        {
            return textBoxName.Text;
        }

        public string MapAuthor()
        {
            return textBoxAuthor.Text;
        }

        public void Apply()
        {
            if (mapModel == null)
                return;
            MapModificationHelper.Rename(ref mapModel, MapName(), MapAuthor());
        }

        private void MapInfoWidget_Load(object sender, EventArgs e)
        {
            TranslationHelper.Instance().TranslationChanged += ReloadTranslation;
            if (pictureBox1.Width < pictureBox1.Height)
            {
                pictureBox1.Height = pictureBox1.Width;
            }
            else
            {
                pictureBox1.Width = pictureBox1.Height;
            }
            int posX = Width - pictureBox1.Width;
            posX /= 2;
            pictureBox1.Location = new Point(posX, pictureBox1.Location.Y);
        }
    }
}
