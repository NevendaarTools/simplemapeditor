﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NevendaarTools;

namespace SimpleMapEditor
{
    public partial class RuinEditorWidget : UserControl
    {
        MapModel mapModel;
        GameDataModel gameModel;

        public class SearchResult
        {
            public string context;
            public D2Ruin ruin;
        }
        List<SearchResult> searchResults = new List<SearchResult>();

        public void ReloadTranslation()
        {

        }

        public RuinEditorWidget()
        {
            InitializeComponent();
            TranslationHelper.Instance().TranslationChanged += ReloadTranslation;
        }

        public void Init(   MapModel mapModel_,
                            GameDataModel gameModel_)
        {
            mapModel = mapModel_;
            gameModel = gameModel_;
            searchResults.Clear();
            resultListBox.Items.Clear();

            List<D2Ruin> ruins = mapModel.GetListByType<D2Ruin>();
            foreach (D2Ruin ruin in ruins)
            {
                searchResults.Add(new SearchResult()
                {
                    ruin = ruin,
                    context = ruin._name + ruin._posX.ToString() + "x" + ruin._posY.ToString()
                });
                resultListBox.Items.Add(searchResults.Last().context);
            }
        }

        private void resultListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = resultListBox.SelectedIndex;
            if (index < 0 || index > searchResults.Count)
                return;
            ruinWidget1.SetRuin(ref mapModel, searchResults[index].ruin, ref gameModel);
        }
    }
}
