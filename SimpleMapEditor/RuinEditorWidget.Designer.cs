﻿namespace SimpleMapEditor
{
    partial class RuinEditorWidget
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.resultListBox = new System.Windows.Forms.ListBox();
            this.ruinWidget1 = new SimpleMapEditor.RuinWidget();
            this.SuspendLayout();
            // 
            // resultListBox
            // 
            this.resultListBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.resultListBox.FormattingEnabled = true;
            this.resultListBox.Location = new System.Drawing.Point(0, 0);
            this.resultListBox.Name = "resultListBox";
            this.resultListBox.Size = new System.Drawing.Size(178, 407);
            this.resultListBox.TabIndex = 1;
            this.resultListBox.SelectedIndexChanged += new System.EventHandler(this.resultListBox_SelectedIndexChanged);
            // 
            // ruinWidget1
            // 
            this.ruinWidget1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ruinWidget1.Location = new System.Drawing.Point(175, 4);
            this.ruinWidget1.Name = "ruinWidget1";
            this.ruinWidget1.Size = new System.Drawing.Size(434, 405);
            this.ruinWidget1.TabIndex = 0;
            // 
            // RuinEditorWidget
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.resultListBox);
            this.Controls.Add(this.ruinWidget1);
            this.Name = "RuinEditorWidget";
            this.Size = new System.Drawing.Size(612, 412);
            this.ResumeLayout(false);

        }

        #endregion

        private RuinWidget ruinWidget1;
        private System.Windows.Forms.ListBox resultListBox;
    }
}
