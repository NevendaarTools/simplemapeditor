﻿namespace SimpleMapEditor
{
    partial class RuinWidget
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.positionLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.descTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.goldNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.undeadNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.empireNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.clansNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.elvesNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.demonsNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.applyButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.goldNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.undeadNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.empireNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clansNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.elvesNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.demonsNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Position:";
            // 
            // positionLabel
            // 
            this.positionLabel.AutoSize = true;
            this.positionLabel.Location = new System.Drawing.Point(71, 4);
            this.positionLabel.Name = "positionLabel";
            this.positionLabel.Size = new System.Drawing.Size(35, 13);
            this.positionLabel.TabIndex = 1;
            this.positionLabel.Text = "label2";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Name:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Description:";
            // 
            // nameTextBox
            // 
            this.nameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nameTextBox.Location = new System.Drawing.Point(74, 27);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(316, 20);
            this.nameTextBox.TabIndex = 4;
            // 
            // descTextBox
            // 
            this.descTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.descTextBox.Location = new System.Drawing.Point(74, 50);
            this.descTextBox.Multiline = true;
            this.descTextBox.Name = "descTextBox";
            this.descTextBox.Size = new System.Drawing.Size(316, 90);
            this.descTextBox.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 158);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Reward:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 179);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Gold:";
            // 
            // goldNumericUpDown
            // 
            this.goldNumericUpDown.Location = new System.Drawing.Point(60, 177);
            this.goldNumericUpDown.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.goldNumericUpDown.Name = "goldNumericUpDown";
            this.goldNumericUpDown.Size = new System.Drawing.Size(88, 20);
            this.goldNumericUpDown.TabIndex = 8;
            // 
            // undeadNumericUpDown
            // 
            this.undeadNumericUpDown.Location = new System.Drawing.Point(208, 179);
            this.undeadNumericUpDown.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.undeadNumericUpDown.Name = "undeadNumericUpDown";
            this.undeadNumericUpDown.Size = new System.Drawing.Size(88, 20);
            this.undeadNumericUpDown.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(154, 181);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Undead:";
            // 
            // empireNumericUpDown
            // 
            this.empireNumericUpDown.Location = new System.Drawing.Point(208, 205);
            this.empireNumericUpDown.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.empireNumericUpDown.Name = "empireNumericUpDown";
            this.empireNumericUpDown.Size = new System.Drawing.Size(88, 20);
            this.empireNumericUpDown.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(154, 207);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Empire:";
            // 
            // clansNumericUpDown
            // 
            this.clansNumericUpDown.Location = new System.Drawing.Point(60, 203);
            this.clansNumericUpDown.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.clansNumericUpDown.Name = "clansNumericUpDown";
            this.clansNumericUpDown.Size = new System.Drawing.Size(88, 20);
            this.clansNumericUpDown.TabIndex = 12;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 205);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(36, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "Clans:";
            // 
            // elvesNumericUpDown
            // 
            this.elvesNumericUpDown.Location = new System.Drawing.Point(208, 231);
            this.elvesNumericUpDown.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.elvesNumericUpDown.Name = "elvesNumericUpDown";
            this.elvesNumericUpDown.Size = new System.Drawing.Size(88, 20);
            this.elvesNumericUpDown.TabIndex = 18;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(154, 233);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(36, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Elves:";
            // 
            // demonsNumericUpDown
            // 
            this.demonsNumericUpDown.Location = new System.Drawing.Point(60, 229);
            this.demonsNumericUpDown.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.demonsNumericUpDown.Name = "demonsNumericUpDown";
            this.demonsNumericUpDown.Size = new System.Drawing.Size(88, 20);
            this.demonsNumericUpDown.TabIndex = 16;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 231);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 13);
            this.label10.TabIndex = 15;
            this.label10.Text = "Demons:";
            // 
            // applyButton
            // 
            this.applyButton.Location = new System.Drawing.Point(208, 270);
            this.applyButton.Name = "applyButton";
            this.applyButton.Size = new System.Drawing.Size(88, 23);
            this.applyButton.TabIndex = 19;
            this.applyButton.Text = "Apply";
            this.applyButton.UseVisualStyleBackColor = true;
            this.applyButton.Click += new System.EventHandler(this.applyButton_Click);
            // 
            // RuinWidget
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.applyButton);
            this.Controls.Add(this.elvesNumericUpDown);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.demonsNumericUpDown);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.empireNumericUpDown);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.clansNumericUpDown);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.undeadNumericUpDown);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.goldNumericUpDown);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.descTextBox);
            this.Controls.Add(this.nameTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.positionLabel);
            this.Controls.Add(this.label1);
            this.Name = "RuinWidget";
            this.Size = new System.Drawing.Size(393, 414);
            ((System.ComponentModel.ISupportInitialize)(this.goldNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.undeadNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.empireNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clansNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.elvesNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.demonsNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label positionLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.TextBox descTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown goldNumericUpDown;
        private System.Windows.Forms.NumericUpDown undeadNumericUpDown;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown empireNumericUpDown;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown clansNumericUpDown;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown elvesNumericUpDown;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown demonsNumericUpDown;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button applyButton;
    }
}
