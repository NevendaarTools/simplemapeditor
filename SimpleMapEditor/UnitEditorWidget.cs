﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NevendaarTools;
using NevendaarTools.DataTypes;

namespace SimpleMapEditor
{
    public partial class UnitEditorWidget : UserControl
    {
        public class SearchResult
        {
            public string context;
            public D2Unit unit;
        }

        GameDataModel gameModel = new GameDataModel();
        MapModel mapModel = new MapModel();
        List<SearchResult> searchResults = new List<SearchResult>();

        public void ReloadTranslation()
        {

        }


        public UnitEditorWidget()
        {
            InitializeComponent();
            TranslationHelper.Instance().TranslationChanged += ReloadTranslation;
        }

        public void Init(MapModel mapModel_,
                         GameDataModel gameModel_)
        {
            mapModel = mapModel_;
            gameModel = gameModel_;
            searchResults.Clear();
            resultListBox.Items.Clear();
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            searchResults.Clear();
            resultListBox.Items.Clear();
            int unitNumber;
            if (int.TryParse(searchTextBox.Text, out unitNumber))
            {
                D2Unit unit = mapModel.GetById<D2Unit>("UN" + unitNumber.ToString("X").PadLeft(4, '0').ToLower());
                if (unit != null)
                {
                    d2UnitWidget1.SetUnit(ref mapModel, unit, ref gameModel);
                    return;
                }
            }
            List<D2Ruin> ruins = mapModel.GetListByType<D2Ruin>();
            List<D2Village> villages = mapModel.GetListByType<D2Village>();
            List<D2Stack> stacks = mapModel.GetListByType<D2Stack>();
            List<D2Capital> capitals = mapModel.GetListByType<D2Capital>();

            foreach (D2Ruin ruin in ruins)
            {
                for (int i = 0; i < 6; i++)
                {
                    if (ruin._unit[i] == "000000")
                        continue;
                    D2Unit ruinUnit = mapModel.GetById<D2Unit>(ruin._unit[i]);
                    if (ruinUnit == null)
                        continue;
                    if (ruinUnit._Type.ToLower() == searchTextBox.Text.ToLower() ||
                        searchTextBox.Text == "")
                    {
                        searchResults.Add(new SearchResult()
                        {
                            context = "Ruin at " +
                            ruin._posX.ToString() + "_" +
                            ruin._posY.ToString() + " pos = " + i.ToString(),
                            unit = ruinUnit
                        });
                    }
                }
            }
            foreach (D2Village ruin in villages)
            {
                for (int i = 0; i < 6; i++)
                {
                    if (ruin._Unit[i] == "000000")
                        continue;
                    D2Unit ruinUnit = mapModel.GetById<D2Unit>(ruin._Unit[i]);
                    if (ruinUnit == null)
                        continue;
                    if (ruinUnit._Type.ToLower() == searchTextBox.Text.ToLower() ||
                        searchTextBox.Text == "")
                    {
                        searchResults.Add(new SearchResult()
                        {
                            context = "Village at " +
                            ruin._PosX.ToString() + "_" +
                            ruin._PosY.ToString() + " pos = " + i.ToString(),
                            unit = ruinUnit
                        });
                    }
                }
            }
            foreach (D2Capital ruin in capitals)
            {
                for (int i = 0; i < 6; i++)
                {
                    if (ruin._Unit[i] == "000000")
                        continue;
                    D2Unit ruinUnit = mapModel.GetById<D2Unit>(ruin._Unit[i]);
                    if (ruinUnit == null)
                        continue;
                    if (ruinUnit._Type.ToLower() == searchTextBox.Text.ToLower() ||
                        searchTextBox.Text == "")
                    {
                        searchResults.Add(new SearchResult()
                        {
                            context = "Capital at " +
                            ruin._PosX.ToString() + "_" +
                            ruin._PosY.ToString() + " pos = " + i.ToString(),
                            unit = ruinUnit
                        });
                    }
                }
            }
            foreach (D2Stack ruin in stacks)
            {
                for (int i = 0; i < 6; i++)
                {
                    if (ruin._Unit[i] == "000000")
                        continue;
                    D2Unit ruinUnit = mapModel.GetById<D2Unit>(ruin._Unit[i]);
                    if (ruinUnit == null)
                        continue;
                    if (ruinUnit._Type.ToLower() == searchTextBox.Text.ToLower() ||
                        searchTextBox.Text == "")
                    {
                        searchResults.Add(new SearchResult()
                        {
                            context = "Stack at " +
                            ruin._posX.ToString() + "_" +
                            ruin._posY.ToString() + " pos = " + i.ToString(),
                            unit = ruinUnit
                        });
                    }
                }
            }
            foreach (SearchResult res in searchResults)
                resultListBox.Items.Add(res.context);
        }

        private void resultListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = resultListBox.SelectedIndex;
            if (index < 0 || index > searchResults.Count)
                return;
            d2UnitWidget1.SetUnit(ref mapModel, searchResults[index].unit, ref gameModel);
        }
    }
}
