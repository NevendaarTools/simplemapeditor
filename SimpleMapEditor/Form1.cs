﻿using NevendaarTools;
using NevendaarTools.DataTypes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SimpleMapEditor
{
    public partial class Form1 : Form
    {
        GameDataModel gameModel = new GameDataModel();
        MapModel mapModel = new MapModel();
        SettingsManager settings = new SettingsManager("settings.xml");
        FolderBrowserDialog browserDialog = new FolderBrowserDialog();
        MapModificationHelper modificationHelper = new MapModificationHelper();
        private MapReader mapReader = new MapReader();
        
        private void Log(string str)
        {
            logTextBox.AppendText(str + "\n");
        }

        public void ReloadTranslation()
        {

        }

        public Form1()
        {
            InitializeComponent();
            ReloadTranslation();
            pathTextBox.Text = settings.Value("GamePath", "");
            TranslationHelper.Instance().TranslationChanged += ReloadTranslation;
            TranslationHelper.Instance().LoadTranslation("ru");
            TranslationHelper.Instance().SetTranslation("ru");
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Файлы карт|*.sg;";
            if (saveFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;

            MapReader.SaveMap(mapModel, saveFileDialog1.FileName);
        }

        private void openButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Filter = "Файлы карт|*.sg;";
            openDialog.InitialDirectory = settings.Value("openPath", ".");
            if (openDialog.ShowDialog() != DialogResult.OK)
                return;
            parseGameButton_Click(sender, e);
            settings.SetValue("openPath", System.IO.Path.GetDirectoryName(openDialog.FileName));
            mapReader.ReadMap(mapModel, openDialog.FileName);
            mapInfoWidget1.SetMap(ref mapModel);
            unitEditorWidget1.Init(mapModel, gameModel);
            ruinEditorWidget1.Init(mapModel, gameModel);
            villageEditorWidget1.Init(mapModel, gameModel);
            

            Bitmap bmp = MapTileHelper.FillHiResImage(ref mapModel, ref gameModel, mapModel.Header._mapSize);
            bmp.Save(openDialog.FileName.Replace(".sg", ".bmp"));
        }

        private void parseGameButton_Click(object sender, EventArgs e)
        {
            logTextBox.Text = "";
            if (!System.IO.File.Exists(pathTextBox.Text + "\\globals\\Tglobal.dbf"))
            {
                Log("Invalid path! File " + pathTextBox.Text + "\\globals\\Tglobal.dbf not found!!!");
                return;
            }
            gameModel.Init(pathTextBox.Text, ref settings);
            settings.SetValue("GamePath", pathTextBox.Text);
            unitInfoWidget1.Init(mapModel, gameModel);
        }

        private void selectPathButton_Click(object sender, EventArgs e)
        {
            if (browserDialog.ShowDialog() == DialogResult.Cancel)
                return;

            pathTextBox.Text = browserDialog.SelectedPath;
        }
    }
}
