﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NevendaarTools;
using NevendaarTools.Transform;

namespace SimpleMapEditor
{
    public partial class RuinWidget : UserControl
    {
        MapModel mapModel;
        D2Ruin ruin;
        GameDataModel gameModel;

        public void ReloadTranslation()
        {

        }

        public RuinWidget()
        {
            InitializeComponent();
            TranslationHelper.Instance().TranslationChanged += ReloadTranslation;
        }

        public void SetRuin(ref MapModel map, D2Ruin ruin_, ref GameDataModel dataModel)
        {
            mapModel = map;
            ruin = ruin_;
            gameModel = dataModel;

            nameTextBox.Text = ruin._name;
            descTextBox.Text = ruin._desc;
            positionLabel.Text = ruin._posX.ToString() + "x" + ruin._posY.ToString();

            goldNumericUpDown.Value = CostHelper.G(ruin._cash);
            undeadNumericUpDown.Value = CostHelper.E(ruin._cash);
            demonsNumericUpDown.Value = CostHelper.R(ruin._cash);
            empireNumericUpDown.Value = CostHelper.Y(ruin._cash);
            elvesNumericUpDown.Value = CostHelper.B(ruin._cash);
            clansNumericUpDown.Value = CostHelper.W(ruin._cash);

            //pictureBox1.Image = MapTileHelper.FillImage(ref map, mapSize);
        }

        public void Apply()
        {
            ruin._name = nameTextBox.Text;
            ruin._desc = descTextBox.Text;
            ruin._cash = CostHelper.Form(
                (int)goldNumericUpDown.Value,
                (int)demonsNumericUpDown.Value,
                (int)empireNumericUpDown.Value,
                (int)undeadNumericUpDown.Value,
                (int)clansNumericUpDown.Value,
                (int)elvesNumericUpDown.Value);
            mapModel.Replace(ruin);
        }

        private void applyButton_Click(object sender, EventArgs e)
        {
            Apply();
        }
    }
}
