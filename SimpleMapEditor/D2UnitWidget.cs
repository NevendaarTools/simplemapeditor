﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NevendaarTools;
using System.IO;

namespace SimpleMapEditor
{
    public partial class D2UnitWidget : UserControl
    {
        MapModel mapModel;
        D2Unit unit;
        GameDataModel gameModel;

        public D2UnitWidget()
        {
            InitializeComponent();
            TranslationHelper.Instance().TranslationChanged += ReloadTranslation;
        }

        public void ReloadTranslation()
        {

        }

        public void SetUnit(ref MapModel map, D2Unit unit_, ref GameDataModel dataModel)
        {
            mapModel = map;
            unit = unit_;
            gameModel = dataModel;

            idLabel.Text = unit._ObjId;
            nameTextBox.Text = unit._Name;
            typeTextBox.Text = unit._Type;
            levelUpDown.Value = unit._Level;
            if (gameModel._Units.ContainsKey(unit._Type.ToLower()))
            {
                GameUnit baseUnit = gameModel._Units[unit._Type.ToLower()];
                baseNameLabel.Text = baseUnit.name;
                descTextBox.Text = baseUnit.desc;
                string unitIconId = (baseUnit.baseUnit == "G000000000") ? unit._Type : baseUnit.baseUnit;
                MemoryStream stream = dataModel._ResourceModel.GetImageRawById(unitIconId.ToUpper() + "FACE");
                if (stream != null)
                    pictureBox1.Image = Image.FromStream(stream);
                else
                    pictureBox1.Image = null;
            }
            
            //pictureBox1.Image = MapTileHelper.FillImage(ref map, mapSize);
        }

        public void Apply()
        {
            unit._Name = nameTextBox.Text;
            unit._Type = typeTextBox.Text;
            unit._Level = (int)levelUpDown.Value;
            mapModel.Replace(unit);
            SetUnit(ref mapModel, unit, ref gameModel);
        }

        private void applyButton_Click(object sender, EventArgs e)
        {
            Apply();
        }
    }
}
