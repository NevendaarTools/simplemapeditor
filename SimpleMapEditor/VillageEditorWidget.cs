﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NevendaarTools;

namespace SimpleMapEditor
{
    public partial class VillageEditorWidget : UserControl
    {
        MapModel mapModel;
        GameDataModel gameModel;
        public class SearchResult
        {
            public string context;
            public D2Village village;
        }
        List<SearchResult> searchResults = new List<SearchResult>();

        public void ReloadTranslation()
        {

        }

        public VillageEditorWidget()
        {
            InitializeComponent();
            TranslationHelper.Instance().TranslationChanged += ReloadTranslation;
        }

        public void Init(MapModel mapModel_,
                            GameDataModel gameModel_)
        {
            mapModel = mapModel_;
            gameModel = gameModel_;
            searchResults.Clear();
            resultListBox.Items.Clear();

            List<D2Village> villages = mapModel.GetListByType<D2Village>();
            foreach (D2Village village in villages)
            {
                searchResults.Add(new SearchResult()
                {
                    village = village,
                    context = village._Name + village._PosX.ToString() + "x" + village._PosY.ToString()
                });
                resultListBox.Items.Add(searchResults.Last().context);
            }
        }

        private void resultListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = resultListBox.SelectedIndex;
            if (index < 0 || index > searchResults.Count)
                return;
            vilageWidget1.SetObject(ref mapModel, searchResults[index].village, ref gameModel);
        }
    }
}
