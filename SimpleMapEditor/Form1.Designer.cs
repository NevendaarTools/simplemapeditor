﻿namespace SimpleMapEditor
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.parseGameButton = new System.Windows.Forms.Button();
            this.selectPathButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.pathTextBox = new System.Windows.Forms.TextBox();
            this.openButton = new System.Windows.Forms.Button();
            this.logTextBox = new System.Windows.Forms.TextBox();
            this.saveButton = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.unitEditorWidget1 = new SimpleMapEditor.UnitEditorWidget();
            this.ruinEditorWidget1 = new SimpleMapEditor.RuinEditorWidget();
            this.villageEditorWidget1 = new SimpleMapEditor.VillageEditorWidget();
            this.unitInfoWidget1 = new SimpleMapEditor.UnitInfoWidget();
            this.mapInfoWidget1 = new NevedaarWidgets.MapInfoWidget();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // parseGameButton
            // 
            this.parseGameButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.parseGameButton.Location = new System.Drawing.Point(1100, 9);
            this.parseGameButton.Name = "parseGameButton";
            this.parseGameButton.Size = new System.Drawing.Size(111, 23);
            this.parseGameButton.TabIndex = 22;
            this.parseGameButton.Text = "Parse";
            this.parseGameButton.UseVisualStyleBackColor = true;
            this.parseGameButton.Click += new System.EventHandler(this.parseGameButton_Click);
            // 
            // selectPathButton
            // 
            this.selectPathButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.selectPathButton.Location = new System.Drawing.Point(1065, 9);
            this.selectPathButton.Name = "selectPathButton";
            this.selectPathButton.Size = new System.Drawing.Size(29, 23);
            this.selectPathButton.TabIndex = 21;
            this.selectPathButton.Text = "...";
            this.selectPathButton.UseVisualStyleBackColor = true;
            this.selectPathButton.Click += new System.EventHandler(this.selectPathButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Game path:";
            // 
            // pathTextBox
            // 
            this.pathTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pathTextBox.Location = new System.Drawing.Point(84, 11);
            this.pathTextBox.Name = "pathTextBox";
            this.pathTextBox.Size = new System.Drawing.Size(975, 20);
            this.pathTextBox.TabIndex = 19;
            this.pathTextBox.Text = "E:\\Games\\Game\\globals";
            // 
            // openButton
            // 
            this.openButton.Location = new System.Drawing.Point(10, 39);
            this.openButton.Name = "openButton";
            this.openButton.Size = new System.Drawing.Size(121, 23);
            this.openButton.TabIndex = 24;
            this.openButton.Text = "Open map";
            this.openButton.UseVisualStyleBackColor = true;
            this.openButton.Click += new System.EventHandler(this.openButton_Click);
            // 
            // logTextBox
            // 
            this.logTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.logTextBox.Location = new System.Drawing.Point(10, 405);
            this.logTextBox.Multiline = true;
            this.logTextBox.Name = "logTextBox";
            this.logTextBox.Size = new System.Drawing.Size(263, 261);
            this.logTextBox.TabIndex = 25;
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(198, 37);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 23);
            this.saveButton.TabIndex = 26;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(280, 39);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(931, 627);
            this.tabControl1.TabIndex = 27;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.unitEditorWidget1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(923, 601);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Units";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.ruinEditorWidget1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(923, 601);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Ruins";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.villageEditorWidget1);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(923, 601);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Villages";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.unitInfoWidget1);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(923, 601);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "tabPage4";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // unitEditorWidget1
            // 
            this.unitEditorWidget1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.unitEditorWidget1.Location = new System.Drawing.Point(0, 0);
            this.unitEditorWidget1.Name = "unitEditorWidget1";
            this.unitEditorWidget1.Size = new System.Drawing.Size(1025, 595);
            this.unitEditorWidget1.TabIndex = 0;
            // 
            // ruinEditorWidget1
            // 
            this.ruinEditorWidget1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ruinEditorWidget1.Location = new System.Drawing.Point(0, 0);
            this.ruinEditorWidget1.Name = "ruinEditorWidget1";
            this.ruinEditorWidget1.Size = new System.Drawing.Size(684, 566);
            this.ruinEditorWidget1.TabIndex = 0;
            // 
            // villageEditorWidget1
            // 
            this.villageEditorWidget1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.villageEditorWidget1.Location = new System.Drawing.Point(4, 4);
            this.villageEditorWidget1.Name = "villageEditorWidget1";
            this.villageEditorWidget1.Size = new System.Drawing.Size(674, 569);
            this.villageEditorWidget1.TabIndex = 0;
            // 
            // unitInfoWidget1
            // 
            this.unitInfoWidget1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.unitInfoWidget1.Location = new System.Drawing.Point(4, 4);
            this.unitInfoWidget1.Name = "unitInfoWidget1";
            this.unitInfoWidget1.Size = new System.Drawing.Size(913, 527);
            this.unitInfoWidget1.TabIndex = 0;
            // 
            // mapInfoWidget1
            // 
            this.mapInfoWidget1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.mapInfoWidget1.Location = new System.Drawing.Point(11, 65);
            this.mapInfoWidget1.Margin = new System.Windows.Forms.Padding(0);
            this.mapInfoWidget1.Name = "mapInfoWidget1";
            this.mapInfoWidget1.Size = new System.Drawing.Size(265, 336);
            this.mapInfoWidget1.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1217, 670);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.logTextBox);
            this.Controls.Add(this.openButton);
            this.Controls.Add(this.parseGameButton);
            this.Controls.Add(this.selectPathButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pathTextBox);
            this.Controls.Add(this.mapInfoWidget1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private NevedaarWidgets.MapInfoWidget mapInfoWidget1;
        private System.Windows.Forms.Button parseGameButton;
        private System.Windows.Forms.Button selectPathButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox pathTextBox;
        private System.Windows.Forms.Button openButton;
        private System.Windows.Forms.TextBox logTextBox;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private UnitEditorWidget unitEditorWidget1;
        private RuinEditorWidget ruinEditorWidget1;
        private System.Windows.Forms.TabPage tabPage3;
        private VillageEditorWidget villageEditorWidget1;
        private System.Windows.Forms.TabPage tabPage4;
        private UnitInfoWidget unitInfoWidget1;
    }
}

